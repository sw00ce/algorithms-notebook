{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Convolution Introduction\n",
    "Suppose we have two vectors representing polynomials:  \n",
    "a = [3, 1 ,2] = $3 + 1x + 2x^2$  \n",
    "b = [4, 1, 5] = $4 + 1x + 5x^2$  \n",
    "$a \\circledast b = c$  \n",
    "$c$ is the _convolution_ ($\\circledast$) of $a$ and $b$  \n",
    "\n",
    "We find $c_m$ an arbitrary coefficient of $c(x)$ by summing all possible contributions from $a(x)$ and $b(x)$.\n",
    "$$ c_m = \\sum_{i,k:i+k=m} a_i \\cdot b_k = \\sum_i a_i \\cdot b_{m-i} $$  \n",
    "For this example $[3, 1, 2] \\circledast [4, 1, 5] :$\n",
    "$$\n",
    "c_0 = 3 \\cdot 4  \\\\\n",
    "c_1 = 3 \\cdot 1 + 1 \\cdot 4 \\\\\n",
    "c_2 = 3 \\cdot 5 + 1 \\cdot 1 + 2 \\cdot 4 \\\\\n",
    "\\\\\n",
    "c = [c_0, c_1, c_2]\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fast Fourier Transform Algorithms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's define a way to evaluate a polynomial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from time import time\n",
    "\n",
    "def poly_eval(coefs, x):\n",
    "    # Evaluate vector as a polynomial \n",
    "    result = 0\n",
    "    for i in range(len(coefs)):\n",
    "        # For each position in the vector \n",
    "        result += coefs[i] * x**i\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#####  Runtime (of `poly_eval()`)\n",
    "`poly_eval()` is $\\in \\Theta(n)$ \n",
    "* We evaluate for $n$ powers in the polynomial. \n",
    "\n",
    "This will allow us to create the naive version of the FFT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "47"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly_a = [1,3,2,4]\n",
    "poly_eval(poly_a, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Naive FFT\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def naive_fft(vec):\n",
    "    n = len(vec)\n",
    "    for i in range(n):\n",
    "        # 1j as a complex number\n",
    "        # These x's are known as the \"complex roots of unity\"\n",
    "        x = np.exp(-2*np.pi*i*1j/n)\n",
    "        print('a({}) = {}'.format(x, poly_eval(vec,x)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Polynomial vector: [1, 3, 2, 4]\n",
      "Naive FFT: \n",
      "a((1+0j)) = (10+0j)\n",
      "a((6.123233995736766e-17-1j)) = (-1.0000000000000004+0.9999999999999996j)\n",
      "a((-1-1.2246467991473532e-16j)) = (-4-1.3471114790620884e-15j)\n",
      "a((-1.8369701987210297e-16+1j)) = (-0.9999999999999982-1.0000000000000009j)\n",
      "Numpy's FFT implementation: \n",
      "[10.+0.j -1.+1.j -4.+0.j -1.-1.j]\n"
     ]
    }
   ],
   "source": [
    "# Let's check this implementation versus the numpy implementation as a sanity check\n",
    "print(\"Polynomial vector: \" + str(poly_a))\n",
    "print(\"Naive FFT: \")\n",
    "naive_fft(poly_a)\n",
    "print(\"Numpy's FFT implementation: \")\n",
    "print(np.fft.fft(poly_a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looks like we are on the right track! The naive implementation of the FFT we can see is $ \\in \\Theta(n^2) $\n",
    "* We see $n$ iterations of the loop\n",
    "* `poly_eval` operates on a vector of size $n\n",
    "But we can make it smaller using some trickery. For this we introduce the concept of  __packing__."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe this property of a polynomial with only even degrees evaluated for a negative and a positive x."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a(2) = 9\n",
      "a(-2) = 9\n"
     ]
    }
   ],
   "source": [
    "# A polynomial with only even degrees\n",
    "a_even = [1,0,2,0]\n",
    "x = 2\n",
    "print('a({}) = {}'.format(x ,poly_eval(a_even, x)))\n",
    "print('a({}) = {}'.format(-1*x ,poly_eval(a_even, -1*x)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Though, this does not work so conveniently with an only odd degree polynomial, turning an only odd degree polynomial into an only even polynomial is quite simple! Simply strip an $x$ from the odd polynomial to turn it even.  \n",
    "  \n",
    "We should also introduce the `pack()` function which strips the zeroes from the polynomial vector. We need this to make shrink the effort on each recursion. If this were not so, we would be stuck in the recursion tree! \n",
    "\n",
    "$$a(x) = a_{even}(x) + a_{odd}(x) \\\\ \n",
    " = a_{even}(x) + x*u(x) \\\\\n",
    " = pack(a_{even})(x^2) + x*pack(u)(x^2)\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Cooley-Turkey FFT \n",
    "\n",
    "$$r(n) = 2*r(\\frac{n}{2}) + \\Theta(n) \\in \\Theta(n*log(n))$$\n",
    "\n",
    "1. In each recursion, packing allows us to store a coefficient vector of length $\\frac{n}{2}$\n",
    "2. In each recursion call we evaluate $n$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ct_fft(vec):\n",
    "    n=len(vec)\n",
    "    if n==1:\n",
    "        return vec\n",
    "\n",
    "    result = np.array(np.zeros(n), np.complex128)\n",
    "\n",
    "    # packed coefficients eliminate zeros. eg., f(x)=1+2x+3x**2+..., then\n",
    "    # e(x)=1+3x**2+... = 1+0x+3x**2+0x**3+... = (1+3y+...), where y=x**2\n",
    "    packed_evens = vec[::2]\n",
    "    packed_odds  = vec[1::2]\n",
    "\n",
    "    # packed_evens(x**2) and packed_odds(x**2) for the first half of x\n",
    "    # points. The other half of the points are the negatives of the first half\n",
    "    fft_evens = ct_fft(packed_evens)\n",
    "    fft_odds  = ct_fft(packed_odds)\n",
    "\n",
    "    # Butterfly:\n",
    "    for i in range(n//2):\n",
    "        x = np.exp(-2*np.pi*i*1j/n)\n",
    "        # result = evens(x) + x*odds(x),  where x is a complex root of unity\n",
    "        #        = packed_evens(x**2) + x*packed_odds(x**2)\n",
    "        result[i] = fft_evens[i] + x * fft_odds[i]\n",
    "\n",
    "    for i in range(int(n/2),n):\n",
    "        # result = evens(x) + x*odds(x), wher x is a complex root of unity\n",
    "        #        = packed_evens(x**2) + x*packed_odds(x**2)\n",
    "        x=np.exp(-2*np.pi*i*1j/n)\n",
    "        # first half of the points are negative of the second half.\n",
    "        # x_i = -x_{i+n/2}, x_i**2 = x_{i+n/2}**2; therefore\n",
    "        # packed_evens(x_i**2) = packed_evens(x_{i+n/2}**2), and\n",
    "        # packed_odds(x_i**2) = packed_odds(x_{i+n/2}**2)\n",
    "        result[i] = fft_evens[i - n//2] + x * fft_odds[i - n//2]\n",
    "\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's test it out!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "numpy FFT iplementation: [ 8.38656e+06      +0.j         -2.04800e+03+2670176.33412164j\n",
      " -2.04800e+03+1335087.3816625j  ... -2.04800e+03 -890057.3817766j\n",
      " -2.04800e+03-1335087.3816625j  -2.04800e+03-2670176.33412164j]\n",
      "Took: 0.0006785392761230469 seconds. \n",
      "\n",
      "This Cooley-Turkey implementation: [ 8.38656e+06      +0.j         -2.04800e+03+2670176.33412164j\n",
      " -2.04800e+03+1335087.3816625j  ... -2.04800e+03 -890057.3817766j\n",
      " -2.04800e+03-1335087.3816625j  -2.04800e+03-2670176.33412164j]\n",
      "Took: 0.28255319595336914 seconds. \n",
      "\n",
      "Largest error 5.1341893658729555e-09\n"
     ]
    }
   ],
   "source": [
    "# Lets compare the numpy fft implementation and our ct_fft()\n",
    "# Create a polynomial\n",
    "N=2**12\n",
    "polynomial=np.array(np.arange(N),float)\n",
    "\n",
    "# Try it for numpy\n",
    "t1=time()\n",
    "np_result = np.fft.fft(polynomial)\n",
    "t2=time()\n",
    "t = t2 - t1\n",
    "print('numpy FFT iplementation: ' + str(np_result))\n",
    "print('Took: ' + str(t) + ' seconds. \\n')\n",
    "\n",
    "# Try our implementation\n",
    "t1=time()\n",
    "ct_result = ct_fft(polynomial)\n",
    "t2=time()\n",
    "t = t2 - t1\n",
    "print('This Cooley-Turkey implementation: ' + str(ct_result))\n",
    "print('Took: ' + str(t) + ' seconds. \\n')\n",
    "\n",
    "largesterror = max(np.abs(np_result - ct_result))\n",
    "print('Largest error ' + str(largesterror))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
