# Readme
This is a Jupyter notebook to catalog all the fancy algorithms I've learned, as well as some demonstrations of those algorithms in action. 
Its been a good study resource for completing my computer science degree, and potentially yours!  
  
Pull requests welcome! 

##### Sources
https://alg.cs.umt.edu/media/serang-algorithms-in-python.pdf
