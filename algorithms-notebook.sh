#!/bin/bash
PROJECT=./notebook
IP=127.0.0.1
PORT=8998

cd $PROJECT
exec jupyter notebook --ip=$IP --port $PORT --no-browser
echo "Jupyter notebook started on $IP:$PORT."
